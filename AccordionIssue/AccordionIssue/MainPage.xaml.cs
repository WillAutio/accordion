﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.CustomControls;
using Xamarin.Forms;

namespace AccordionIssue
{
    public partial class MainPage : ContentPage
    {
        private readonly AccViewModel _viewModel;

        public MainPage()
        {
            InitializeComponent();

            _viewModel = new AccViewModel();

            this.BindingContext = _viewModel;
            //this.BindingContext = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
        }
        //void Handle_OnClick(object sender, AccordionItemClickEventArgs e)
        //{
        //    var viewCell = ((AccordionItemView)sender).Parent as ViewCell;

        //    viewCell?.ForceUpdateSize();
        //}
    }


}
