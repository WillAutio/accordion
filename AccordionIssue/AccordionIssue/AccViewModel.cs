﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AccordionIssue
{
    public class Contact
    {
        public string Name { get; set; }

        public string Company { get; set; }
    }

    public class AccViewModel 
    {
        public ObservableCollection<Contact> Contacts { get; set; } = new ObservableCollection<Contact>();
        public string test { get; set; } = "Test";

        public AccViewModel()
        {
            Contacts = SetupContacts();
        }

        public ObservableCollection<Contact> SetupContacts()
        {
            ObservableCollection<Contact> contacts = new ObservableCollection<Contact>();
            Contact c1 = new Contact { Name = "Frank", Company = "First Company" };
            contacts.Add(c1);
            Contact c2 = new Contact { Name = "Sue", Company = "Next Company" };
            contacts.Add(c2);
            Contact c3 = new Contact { Name = "Leslie", Company = "Last Company" };
            contacts.Add(c3);
            return contacts;
        }
    }
}
